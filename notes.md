# Project notes

## Battery measurement
We want to use the 2.56V reference because the voltage divider is 10k/10k so our max voltage is
3.7/2 = 1.85V

LiPo cell runs from 3.7V to 2.75V (discharge cutoff)