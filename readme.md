# RGB Ornament

This project is a simple hanging Christmas tree ornament with a neopixel, a microcontroller, and a battery inside. Lots of fun for christmas.

## Folders

### src
This is the source code for the MCU. It's targeted specifically for an ATtiny85 but it'll probably compile for most any AVR with enough flash and RAM with little to no modification.

### pcb
This folder contains the PCB CAD files for the board. This includes schematics, layout, and gerbers.

### cad
This folder contains the CAD files for the ornament bulb itself. It's a simple bulb with just enough inside to hold the battery and PCB.

## Attributions

Many thanks to Josh Levine for his heavily simplified neopixel library that I was able to import. Check it out on [Github](https://github.com/bigjosh/SimpleNeoPixelDemo)!