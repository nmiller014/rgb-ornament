#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include <util/delay.h>

#include "voltage_measurement.h"
#include "dbg_putchar.h"

void voltageMeasurementInit(void)
{
  ADMUX = (1<<ADLAR) | (1<<REFS1) | (1<<MUX1);
  ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1);
  DIDR0 = (1<<ADC2D);
}

uint8_t voltageMeasurementGetRawAdc(void)
{
  ADCSRA |= (1<<ADSC);
  while (ADCSRA & (1 << ADSC) ); // wait till conversion complete

  dbg_putchar(ADCH);

  return ADCH;
}