#ifndef VOLTAGE_MEASUREMENT_H
#define VOLTAGE_MEASUREMENT_H

#include <stdint.h>

void voltageMeasurementInit(void);
uint8_t voltageMeasurementGetRawAdc(void);

#endif