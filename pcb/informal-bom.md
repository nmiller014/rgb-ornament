# Informal BOM

This file represents an in-progress informal bill-of-materials for the PCB. Most of this information will be transferred to a spreadsheet when I start prepping the PCB, but I'm using this to plan.

## PCB BOM
* WS2812B (Digikey 1528-1104-ND)
* ATtiny85 (Sparkfun COM-09378)
* 850 mAh Li-Po battery (Sparkfun PRT-00341)
* 0.1 uF cap (x2)
* 10 uF cap
* 4.7 kOhm resistor
* 470 Ohm resistor
* Shrouded male JST connected (Sparkfun PRT-08612)