#ifndef EASY_NEOPIXEL_H
#define EASY_NEOPIXEL_H

#include <stdint.h>

void ledsetup(void);
void showColor( unsigned char r , unsigned char g , unsigned char b );

#endif //EASY_NEOPIXEL_H