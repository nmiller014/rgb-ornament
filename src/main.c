#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#include <util/delay.h>

#include "easy_neopixel.h"
#include "voltage_measurement.h"
#include "dbg_putchar.h"

#define MIN_ADC_MEASUREMENT (197)

const uint8_t delayTimeMs = 1;
const uint8_t shiftAmount = 4;

static void set_timer(uint8_t milliseconds)
{
  OCR1A = milliseconds;
  TCCR1 |= ((1 << CS13) | (1 << CS12) | (1 << CS11)); // Start timer at Fcpu/8192
}

static void sleep(uint8_t milliseconds)
{
  if (milliseconds == UINT8_MAX)
  {
    // if they use the max int value, go into SLEEP_MODE_PWR_DOWN with
    // interrupts disabled to sleep forever at as low of a current as possible
    cli();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_mode();
  }
  else
  {
    showColor(0, 0, 255);
    // otherwise, go into SLEEP_MODE_IDLE and set the timer to overflow after
    // x milliseconds
    set_timer(milliseconds);
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }

}

static void update(void)
{
  static uint8_t ticks;
  /*
    In order to save space, I computed the low battery threshold as an ADC
    raw value and used that, instead of calculating battery voltage on the
    fly.

    We use a voltage divider with a ratio of 0.3 (R1 = 100k, R2 = 43.5k).

    My "low battery" threshold voltage is 2.8V (0.1V above the min operating
    voltage of the attiny85).

    2.8 * 0.3 = 0.85V

    We use a 1.1V reference, so that's ~77%. 77% of 255 is 197.
  */
  uint8_t adcResult = voltageMeasurementGetRawAdc();

  if (adcResult < MIN_ADC_MEASUREMENT)
  {
    // set the LED to solid RED and go into deep sleep
    showColor(255, 0, 0);
    sleep(UINT8_MAX);
  }
  else
  {
    // do our normal thing
    showColor(0, 255, 0);
  }

  ticks++;
}

int main(void)
{
  cli();

  voltageMeasurementInit();
  ledsetup();
  dbg_tx_init();

  TCCR1 |= (1 << CTC1); // Configure timer 1 for CTC mode
  TIMSK |= (1 << OCIE1A); // Enable CTC interrupt

  sei();

  while (1)
  {
    update();
    _delay_ms(250);
    sleep(250);
  }

  return 0;
}

ISR(TIMER1_COMPA_vect)
{
  TCCR1 &= ~((1 << CS13) | (1 << CS12) | (1 << CS11) | (1 << CS10));
}